package pl.kpo.generics;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 27/02/2015
 * Time: 10:39
 *
 * WARNING - use -Xlint:unchecked compile option to see more details
 */
public class RawGeneric<T> {

  private T t;

  public void set(T t) {
    this.t = t;
  }

  public T getT() {
    return t;
  }

  public String toString() {
    return "RawGeneric " + t + " - " + t.getClass().getName();
  }

  public static void main(String[] args) {
    RawGeneric<String> rawGeneric1 = new RawGeneric<>();
    rawGeneric1.set("String");
    System.out.println(rawGeneric1);

    RawGeneric rawGeneric2 = new RawGeneric();
    rawGeneric2.set("Raw Object");
    System.out.println(rawGeneric2);
  }


}
