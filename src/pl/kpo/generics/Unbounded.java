package pl.kpo.generics;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kpolak on 01/03/15.
 */
public class Unbounded {
  static List<?> unboundedList = new ArrayList<>();
  static List<? extends Number> upperBoundedList = new ArrayList<Integer>();  // this is only to read
  static List<? super Number> lowerBoundedList = new ArrayList<>();
  static List<Object> objectList = new ArrayList<>();

  public static void main(String[] args) {
    // unboundedList.add(new Integer(0));   IMPOSSIBLE
    unboundedList.add(null);             // ONLY NULL IS POSSIBLE

    lowerBoundedList.add(new Integer(3));
    lowerBoundedList.add(new Double(3.0));


    printAll(lowerBoundedList);


  }


  public static void printAll(List<?> list) {
    for (Object o : list) {
      if (o != null) {
        System.out.println("Element " + o);
      }
    }
  }

}
