package pl.kpo.generics;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.Predicate;

/**
 * Created by kpolak on 01/03/15.
 */
public class GenericMethod2 {

  public static void main(String[] args) {
    Collection<Integer> ci = Arrays.asList(1, 2, 3, 4, 5, 6);
    int count = Algorithm.countIf(ci, p -> p % 2 != 0); //new OddPredicate());
    System.out.println("Number of odd integers = " + count);
  }

}

final class Algorithm {
  public static <T> int countIf(Collection<T> c, Predicate<T> p) {
    int count = 0;
    for (T elem : c)
      if (p.test(elem))
        ++count;
    return count;
  }
}

//interface UnaryPredicate<T> {
//  public boolean test(T obj);
//}
//
//class OddPredicate implements UnaryPredicate<Integer> {
//  public boolean test(Integer i) { return i % 2 != 0; }
//}