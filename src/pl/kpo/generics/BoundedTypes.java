package pl.kpo.generics;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 27/02/2015
 * Time: 17:00
 */


public class BoundedTypes<T extends MyInteger & Countable & Multipliable> {

  T t;

  public BoundedTypes() {}

  public BoundedTypes(T t) {
    this.t = t;
  }

  public static void main(String[] args) {
    BoundedTypes<BTImpl> bt = new BoundedTypes<>();
    bt.t.setMyInt(6);
    System.out.println("MyInt = " + bt.t.getMyInt());
  }
}

class BTImpl extends MyInteger implements Countable, Multipliable {

 public BTImpl(Integer i) {
   super();
   setMyInt(i);
 }

 @Override
  public int count(Integer i1) {
    return 0;
  }

  @Override
  public int multiply(Integer i1) {
    return 0;
  }
}

class MyInteger {
  int myInt;

  public void setMyInt(Integer i) {
    myInt = i;
  }

  public int getMyInt() {
    return myInt;
  }
}


interface Countable {
  int count(Integer i1);
}

interface Multipliable {
  int multiply(Integer i1);
}
