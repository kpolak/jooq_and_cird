package com.ge.mytests.rowscn;

//import org.joda.time.DateTime;
import org.jooq.*;
import org.jooq.impl.DSL;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static com.ge.dbgen.Tables.TMP_KPO_SCN;
import static org.jooq.impl.DSL.max;

/**
 * JOOQ ora_rowscn demo.
 * User: Krzysztof Polak
 * Date: 09/01/2015
 * Time: 11:43
 */
public class JooqRowScnTest {

  private Connection conn;

  public static Field<Long> oraRowScn() {
    return DSL.field("ora_rowscn", Long.class);
  }

  public static void main(String[] args) throws SQLException {

    JooqRowScnTest test = new JooqRowScnTest();

    test.establishConnection();

    test.doInsert();

    test.doRead();

    test.doCommit();

    test.doRead();

    test.releaseConnection();
  }

  /**
   * Read all the rows from the table.
   */
  private void doRead() {
    System.out.println("\nReading data ...");
    DSLContext create = DSL.using(conn, SQLDialect.ORACLE10G);

//    List < TmpKpoScn > rows = create.select(TMP_KPO_SCN.X, TMP_KPO_SCN.INSERT_TIME,
//                         oraRowScn()).from(TMP_KPO_SCN).fetch().into(TmpKpoScn.class);

/*
    Result<Record3<BigDecimal, DateTime, Long>> rows = create.select(TMP_KPO_SCN.X, TMP_KPO_SCN.INSERT_TIME,
                                                                     oraRowScn()).from(TMP_KPO_SCN).orderBy(
        TMP_KPO_SCN.X).fetch();

    for (Record3<BigDecimal, DateTime, Long> r : rows) {
      BigDecimal id = r.value1();
      org.joda.time.DateTime insertTime = r.value2();
      Long rowScn = r.value3();


      System.out.println("X: " + id + " insert time: " + insertTime + " commit time: " + rowScn);

    }
*/
  }

  /**
   * Insert row into the table.
   */
  private void doInsert() throws SQLException {
    conn.setAutoCommit(false);
    System.out.println("\nInserting new row ...");
    DSLContext create = DSL.using(conn, SQLDialect.ORACLE10G);

    // first select the max x
    BigDecimal maxX = create.select(max(TMP_KPO_SCN.X)).from(TMP_KPO_SCN).fetchOne().into(BigDecimal.class);
    BigDecimal newX = maxX.add(new BigDecimal(1));
    System.out.println("New x: " + newX);

//    Record record = create.insertInto(TMP_KPO_SCN, TMP_KPO_SCN.X).values(new BigDecimal(newX.intValue())).returning(
//        TMP_KPO_SCN.INSERT_TIME).fetchOne();

//    System.out.println(record.getValue("Inserted row " + newX + " at " + TMP_KPO_SCN.INSERT_TIME));
  }


  /**
   * Commits database.
   */
  private void doCommit() {
    System.out.println("\nCommit ...");
    try {
      conn.commit();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }


  /**
   * Establish connection.
   */
  private void establishConnection() {

    System.out.println("\nEstablishing connection...");

    String userName = "medora";
    String password = "medora";
    String url = "jdbc:oracle:thin:@deulmrdvvm13:1521:RAD";

    try {
      conn = DriverManager.getConnection(url, userName, password);
    }

    // For the sake of this tutorial, let's keep exception handling simple
    catch (Exception e) {
      e.printStackTrace();
    }
  }


  /**
   * Release connection.
   */
  private void releaseConnection() {
    System.out.println("\nClosing connection...");
    try {
      if (conn != null) {
        conn.close();
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

}
