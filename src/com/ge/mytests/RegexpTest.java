package com.ge.mytests;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 28/11/2014
 * Time: 14:58
 */
public class RegexpTest {

  public static void main(String[] args) {
    Pattern pattern = Pattern.compile("HCE-[a-zA-Z0-9\\.]*");
    String hostname = "HCE-BC0Z9T1";
    String hostname2 = "HCE-bc0z9t1.clients.em.health.ge.com";

    Matcher matcher = pattern.matcher(hostname2);

    if (matcher.matches()) {
      System.out.println("Matched");
    } else {
      System.out.println("Not matched");
    }



  }

}
