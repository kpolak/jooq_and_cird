package com.ge.mytests;

import com.ge.mytests.util.CirdUtil;

import java.net.UnknownHostException;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 09/12/2014
 * Time: 09:18
 */
public class CirdUtilTest {

  private String cidr =     "192.168.190.41/32";
  private String address1 = "192.168.190.41";

  public static void main(String[] args) throws UnknownHostException {
    CirdUtilTest test = new CirdUtilTest();
    test.doTest();
  }

  private void doTest() throws UnknownHostException {
    CirdUtil cirdUtil = new CirdUtil(cidr);
    boolean isInRange = cirdUtil.isInRange(address1);
    System.out.println("Is in range? " + isInRange);
  }
}
